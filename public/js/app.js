$(function () {
    $('.offer').popover({
        placement: 'bottom',
        content: function () {
            $("#offerFormForm").attr('action', "/api/account/publications/" + $(this).parent().find('.offer').data("pub-id") + "/barters")
            $("#barter_receiver_id").val($(this).parent().find('.offer').data("publisher-id"))
            return $("#offerForm").html()
        },
        html: true,
        sanitize: false,
        container: 'body'
    });

    // $("html").on("mouseup", function (e) {
    //     var l = $(e.target);
    //     if (l[0].className.indexOf("popover") == -1) {
    //         $(".popover").each(function () {
    //             $(this).popover("hide");
    //         });
    //     }
    // });
})