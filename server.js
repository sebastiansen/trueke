const express = require('express')
const app = express()
const port = 3000
const morgan = require('morgan')
const path = require('path');
const _ = require('lodash');
const fileUpload = require('express-fileupload');

// Middleware
const bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('combined'))

// Utils
const bcrypt = require('bcrypt')
const uuidv4 = require('uuid/v4')
const Pool = require('pg').Pool
const pool = new Pool({
    user: 'me',
    host: 'localhost',
    database: 'Trueke',
    password: 'password',
    port: 5432,
})
app.use(fileUpload());


// Auth
var passport = require("passport");
const LocalStrategy = require('passport-local').Strategy;
app.use(require('cookie-parser')());
var session = require("express-session");
app.use(session({ secret: "keyboard cat", resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function (user, cb) {
    cb(null, user)
});
passport.deserializeUser(function (obj, cb) {
    cb(null, obj)
});
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, (email, password, done) => {
    (async () => {
        try {
            const user = await findUserByEmail(email)
            if (!user) return done(null, false)
            if (comparePass(password, user.password)) {
                return done(null, user)
            } else {
                return done(null, false)
            }
        } catch (e) {
            done(e)
        }
    })().catch(e => console.error(e.stack))
}))

function comparePass(userPassword, databasePassword) {
    return bcrypt.compareSync(userPassword, databasePassword);
}

function loginRequired(req, res, next) {
    if (!req.user) return res.status(401).json({ msg: 'Please log in' });
    return next();
}

// DB
async function findUserByEmail(email) {
    const { rows } = await pool.query('SELECT * FROM "users" WHERE "email"=$1', [email])
    return rows[0];
}

async function createUser(user) {
    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync(user.password, salt);
    await pool.query(
        'INSERT INTO users (id, firstName, lastName, email, password) VALUES ($1, $2, $3, $4, $5)',
        [uuidv4(), user.firstName, user.lastName, user.email, hash]
    )
}

async function findPublications(search, publisherId) {
    const _search = search ? '%' + search + '%' : '%'
    const query =
        "SELECT p.*, concat(u.firstname,' ',u.lastname) as user_name, count(b.id) as barters_count " +
        "FROM publications p " +
        "LEFT OUTER JOIN barters b ON (b.offering_id = p.id OR b.exchange_id = p.id) AND b.status = 'started' " +
        "INNER JOIN users u ON p.publisher_id = u.id " +
        "WHERE bartered_at IS NULL AND " + (publisherId ? "publisher_id = $2 AND" : "") + " (title like $1 OR description like $1) " +
        "GROUP BY p.id, u.id ORDER BY created_at DESC"
    const params = publisherId ? [_search, publisherId] : [_search]
    const { rows } = await pool.query(query, params)
    return rows
}

async function findOfferings(exchangeId) {
    const { rows } = await pool.query(
        "SELECT p.*, b.id as barter_id " +
        "FROM barters b INNER JOIN publications p ON b.offering_id = p.id " +
        "WHERE exchange_id = $1 ORDER BY updated_at DESC",
        [exchangeId]
    )
    return rows
}

async function findExchanges(offeringId) {
    const { rows } = await pool.query(
        "SELECT p.*" +
        "FROM barters b INNER JOIN publications p ON b.exchange_id = p.id " +
        "WHERE offering_id = $1 ORDER BY updated_at DESC",
        [offeringId]
    )
    return rows
}

async function findBartersByPublication(publicationId) {
    const { rows } = await pool.query(
        "SELECT *, CASE WHEN(offering_id = $1) THEN 'offering' ELSE 'exchange' END AS source " +
        "FROM barters WHERE offering_id = $1 OR exchange_id = $1 ORDER BY updated_at DESC",
        [publicationId]
    )
    return rows
}

async function findBarter(publicationId, exchangeId) {
    const { rows } = await pool.query(
        "SELECT * FROM barters WHERE (offering_id = $1 AND exchange_id = $2) OR (offering_id = $2 AND exchange_id = $1)",
        [publicationId, exchangeId]
    )
    return rows[0]
}

async function findBarterById(id) {
    const { rows } = await pool.query("SELECT * FROM barters WHERE id = $1", [id])
    return rows[0]
}

async function findPublicationById(id) {
    const { rows } = await pool.query(
        "SELECT p.*, u.firstname as user_first_name, u.lastname as user_last_name " +
        "FROM publications p INNER JOIN users u ON p.publisher_id = u.id WHERE p.id = $1",
        [id]
    )
    return rows[0]
}

async function chatHistory(user1Id, user2Id) {
    const { rows } = await pool.query(
        "SELECT * FROM messages WHERE (sender_id = $1 AND receiver_id = $2) OR (sender_id = $2 AND receiver_id = $1) ORDER BY sent_at DESC",
        [user1Id, user2Id]
    )
    return rows
}

async function chatsByUser(userId) {
    const { rows } = await pool.query(
        "SELECT u.id, u.firstname, u.lastname, max(m.sent_at) AS sent_at " +
        "FROM messages m INNER JOIN users u ON (u.id = m.sender_id OR u.id = m.receiver_id) AND u.id <> $1 " +
        "WHERE m.sender_id = $1 OR m.receiver_id = $1 " +
        "GROUP BY u.id, u.firstname, u.lastname " +
        "ORDER BY sent_at DESC",
        [userId]
    )
    return rows
}

app.post('/api/signup', async (req, res, next) => {
    let user = req.body
    try {
        if (await findUserByEmail(user.email)) {
            res.status(400).send({ msg: 'Email already exists' })
        }
        else {
            await createUser(user)
            passport.authenticate('local', (err, user, info) => {
                if (err) { next(err) }
                if (user) {
                    req.logIn(user, function (err) {
                        if (err) { next(err) }
                        res.status(201).redirect('/');
                    });
                }
            })(req, res, next);
        }
    } catch (e) {
        next(e)
    }
})

app.post('/api/login', (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if (err) { next(err) }
        if (!user) { res.status(404).json({ msg: 'User not found' }) }
        if (user) {
            req.logIn(user, function (err) {
                if (err) { next(err) }
                res.status(201).redirect('/');
            });
        }
    })(req, res, next);
});

app.get('/api/logout', function (req, res) {
    req.logout();
    res.status(200).redirect('/')
})

app.get('/api/publications', loginRequired, async (req, res, next) => {
    try {
        const pubs = await findPublications(req.query.search)
        res.status(200).json(pubs)
    } catch (e) {
        next(e)
    }
})

app.get('/api/account/publications', loginRequired, async (req, res, next) => {
    try {
        const pubs = await findPublications(req.query.search, req.user.id)
        res.status(200).json(pubs)
    } catch (e) {
        next(e)
    }
})

app.get('/api/publications/:id/barters', loginRequired, async (req, res, next) => {
    try {
        const barters = await findBartersByPublication(req.params.id)
        res.status(200).json(barters)
    } catch (e) {
        next(e)
    }
})

app.post('/api/publications', loginRequired, async (req, res, next) => {
    const pub = req.body
    const now = new Date()
    const id = uuidv4()
    const image_url = 'public/' + id + "-" + req.files.image.name
    req.files.image.mv(image_url)
    pool.query(
        'INSERT INTO publications (id, publisher_id, title, description, image_url, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)',
        [id, req.user.id, pub.title, pub.description, image_url, now, now],
        (err, results) => {
            if (err) {
                next(err)
            } else {
                res.status(200).redirect('/')
            }
        }
    )
})

//started -> accepted | rejected
const BarterStatus = Object.freeze({ started: "started", accepted: "accepted", rejected: "rejected" })
app.post('/api/account/publications/:exchange_id/barters/:barter_id', loginRequired, async (req, res, next) => {
    const status = BarterStatus[req.body.status]
    // only accepted or rejected
    if (!status && status != BarterStatus.started) {
        res.status(400).send({ msg: 'Invalid status' })
    } else {
        const client = await pool.connect()
        try {
            await client.query('BEGIN')
            // check publication and barter exists
            const barter = await findBarterById(req.params.barter_id)
            const exchange = await findPublicationById(req.params.exchange_id)
            const offering = await findPublicationById(barter.offering_id)
            if (!exchange || !barter || (barter.offering_id != exchange.id && barter.exchange_id != exchange.id)) {
                return res.status(404).send({ msg: "Barter not found" })
            }
            // check barter still in started
            if (barter.status !== BarterStatus.started) {
                return res.status(400).send({ msg: 'Barter ended' })
            }

            const isOfferingOwner = offering.publisher_id === req.user.id;
            const isExchangeOwner = exchange.publisher_id === req.user.id;

            // check user owns one of the publications
            if (!isExchangeOwner && !isOfferingOwner) {
                return res.status(403).send({ msg: "User is not allowed to update barter" })
            }

            // check offering owner can only reject
            if (isOfferingOwner && status !== BarterStatus.rejected) {
                return res.status(403).send({ msg: "Can only accept or reject offerings" })
            }

            const now = new Date()
            await client.query('UPDATE barters SET status = $1, updated_at = $2 WHERE id = $3', [status, now, barter.id])

            if (status === BarterStatus.accepted) {
                // reject all other barters for offer and offered
                await client.query(
                    'UPDATE barters SET status = $1, updated_at = $2 WHERE offering_id = ANY($3::uuid[]) OR exchange_id = ANY($3::uuid[])',
                    [BarterStatus.rejected, now, [barter.offering_id, barter.exchange_id]]
                )
                await client.query(
                    'UPDATE publications SET bartered_at = $1, updated_at = $2 WHERE id = ANY($3::uuid[])',
                    [now, now, [barter.offering_id, barter.exchange_id]]
                )
                await client.query('COMMIT')
                res.status(200).redirect('/account/publications')
            }
        }
        catch (e) {
            await client.query('ROLLBACK')
            next(e)
        } finally {
            client.release()
        }
    }
})

app.post('/api/account/publications/:id/barters', loginRequired, async (req, res, next) => {
    const barter = req.body
    // TOOD: check offering is owned by user
    if (barter.offering_id === req.params.id) {
        res.status(400).send({ msg: 'Cannot barter with itself' })
    } else if (await findBarter(req.params.id, barter.offering_id)) {
        res.status(400).send({ msg: 'Barter already exists' })
    }
    else {
        const client = await pool.connect()
        const now = new Date()
        try {
            await client.query('BEGIN')

            await client.query(
                'INSERT INTO barters (id, offering_id, exchange_id, status, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6)',
                [uuidv4(), barter.offering_id, req.params.id, BarterStatus.started, now, now]
            )

            await client.query(
                'INSERT INTO messages (id, sender_id, receiver_id, sent_at, message) VALUES ($1, $2, $3, $4, $5)',
                [uuidv4(), req.user.id, barter.receiver_id, now, barter.message]
            )

            await client.query('COMMIT')
            res.status(200).redirect('/publications/' + barter.offering_id)
        }
        catch (e) {
            await client.query('ROLLBACK')
            next(e)
        } finally {
            client.release()
        }
    }
})

app.get('/', async function (req, res, next) {
    if (req.user) {
        try {
            const search = req.query.search
            const pubs = await findPublications(search)
            const my_pubs = await findPublications(null, req.user.id)
            const p = _.chunk(pubs, 4)
            res.render('signed-in/index', { publications: p, search: search, user: req.user, my_pubs: my_pubs })
        } catch (e) {
            next(e)
        }
    } else {
        res.render('index')
    }
})

app.get('/account/publications', loginRequired, async function (req, res) {
    try {
        const search = req.query.search
        const pubs = await findPublications(search, req.user.id)
        res.render('signed-in/publications-new', { user: req.user, publications: pubs })
    } catch (e) {
        next(e)
    }
})

app.get('/publications/:id', loginRequired, async function (req, res, next) {
    try {
        const my_pubs = await findPublications(null, req.user.id)
        const pub = await findPublicationById(req.params.id)
        const offerings = await findOfferings(req.params.id)
        const exchanges = await findExchanges(req.params.id)
        res.render('signed-in/publication', { user: req.user, publication: pub, offerings: offerings, my_pubs: my_pubs, exchanges: exchanges })
    } catch (e) {
        next(e)
    }
})

app.get('/account/messages', loginRequired, async function (req, res, next) {
    try {
        const chats = await chatsByUser(req.user.id)
        res.redirect('/account/messages/' + chats[0].id)
    } catch (e) {
        next(e)
    }
})

app.get('/account/messages/:id', loginRequired, async function (req, res, next) {
    try {
        const chats = await chatsByUser(req.user.id)
        const chat = chats.filter(c => c.id === req.params.id)[0]
        const otherChats = chats.filter(c => { return c.id !== req.params.id })
        var history = (await chatHistory(req.user.id, req.params.id)).reverse().map(m => {
            m.isIncoming = m.sender_id !== req.user.id
            return m
        })
        res.render('signed-in/messages', { user: req.user, chats: otherChats, history: history, chat: chat })
    } catch (e) {
        next(e)
    }
})

app.post('/account/messages/:id', loginRequired, async function (req, res, next) {
    try {
        const msg = req.body
        const now = new Date()
        await pool.query(
            'INSERT INTO messages (id, sender_id, receiver_id, sent_at, message) VALUES ($1, $2, $3, $4, $5)',
            [uuidv4(), req.user.id, req.params.id, now, msg.message]
        )
        res.redirect('/account/messages/' + req.params.id)
    } catch (e) {
        next(e)
    }
})

app.get('/signup', function (req, res) { res.render('signup') })

app.get('/login', function (req, res) { res.render('login') })

app.set('view engine', 'pug')

app.use("/public", express.static(path.join(__dirname, 'public')));

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})