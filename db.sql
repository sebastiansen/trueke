PGDMP          /                w            Trueke    11.3    11.2     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16385    Trueke    DATABASE     f   CREATE DATABASE "Trueke" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE "Trueke";
             me    false            �            1259    16451    barters    TABLE       CREATE TABLE public.barters (
    id uuid NOT NULL,
    offering_id uuid NOT NULL,
    exchange_id uuid NOT NULL,
    status character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
    DROP TABLE public.barters;
       public         me    false            �            1259    16401    messages    TABLE     �   CREATE TABLE public.messages (
    id uuid NOT NULL,
    sender_id uuid NOT NULL,
    receiver_id uuid NOT NULL,
    sent_at timestamp with time zone NOT NULL,
    message text
);
    DROP TABLE public.messages;
       public         me    false            �            1259    16386    publications    TABLE     P  CREATE TABLE public.publications (
    id uuid NOT NULL,
    publisher_id uuid NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    image_url character varying(255),
    title character varying(255) NOT NULL,
    description text,
    bartered_at timestamp with time zone
);
     DROP TABLE public.publications;
       public         me    false            �            1259    16391    users    TABLE     �   CREATE TABLE public.users (
    id uuid NOT NULL,
    firstname character(64) NOT NULL,
    lastname character(64) NOT NULL,
    email character(128) NOT NULL,
    password character(60) NOT NULL
);
    DROP TABLE public.users;
       public         me    false                       2606    16455    barters barters_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.barters
    ADD CONSTRAINT barters_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.barters DROP CONSTRAINT barters_pkey;
       public         me    false    199            �           2606    16431    users email 
   CONSTRAINT     G   ALTER TABLE ONLY public.users
    ADD CONSTRAINT email UNIQUE (email);
 5   ALTER TABLE ONLY public.users DROP CONSTRAINT email;
       public         me    false    197                       2606    16405    messages messages_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_pkey;
       public         me    false    198            �           2606    16390    publications publications_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.publications
    ADD CONSTRAINT publications_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.publications DROP CONSTRAINT publications_pkey;
       public         me    false    196                        2606    16395    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         me    false    197            	           2606    16461    barters exchange    FK CONSTRAINT     z   ALTER TABLE ONLY public.barters
    ADD CONSTRAINT exchange FOREIGN KEY (exchange_id) REFERENCES public.publications(id);
 :   ALTER TABLE ONLY public.barters DROP CONSTRAINT exchange;
       public       me    false    3068    196    199                       2606    16456    barters offering    FK CONSTRAINT     z   ALTER TABLE ONLY public.barters
    ADD CONSTRAINT offering FOREIGN KEY (offering_id) REFERENCES public.publications(id);
 :   ALTER TABLE ONLY public.barters DROP CONSTRAINT offering;
       public       me    false    3068    199    196                       2606    16396    publications publisher    FK CONSTRAINT     z   ALTER TABLE ONLY public.publications
    ADD CONSTRAINT publisher FOREIGN KEY (publisher_id) REFERENCES public.users(id);
 @   ALTER TABLE ONLY public.publications DROP CONSTRAINT publisher;
       public       me    false    3072    197    196                       2606    16411    messages receiver    FK CONSTRAINT     t   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT receiver FOREIGN KEY (receiver_id) REFERENCES public.users(id);
 ;   ALTER TABLE ONLY public.messages DROP CONSTRAINT receiver;
       public       me    false    3072    197    198                       2606    16406    messages sender    FK CONSTRAINT     p   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT sender FOREIGN KEY (sender_id) REFERENCES public.users(id);
 9   ALTER TABLE ONLY public.messages DROP CONSTRAINT sender;
       public       me    false    197    3072    198           